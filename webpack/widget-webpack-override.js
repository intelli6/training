module.exports = function (webpackConfig) {
  const path = require('path');
  /**
   * If you need to change the widget webpack config, you can change the webpack config here and return the changed config.
   */
  Object.assign(webpackConfig.resolve.alias, {
    lodash: path.resolve(
      __dirname,
      '../your-extensions/node_plugin/node_modules/lodash'
    ),
    'date-fns': path.resolve(
      __dirname,
      '../your-extensions/node_plugin/node_modules/date-fns'
    ),
    axios: path.resolve(
      __dirname,
      '../your-extensions/node_plugin/node_modules/axios'
    ),
    '@mui/material': path.resolve(
      __dirname,
      '../your-extensions/node_plugin/node_modules/@mui/material'
    ),
    '@mui/icons-material': path.resolve(
      __dirname,
      '../your-extensions/node_plugin/node_modules/@mui/icons-material'
    ),
    '@mui/x-date-pickers': path.resolve(
      __dirname,
      '../your-extensions/node_plugin/node_modules/@mui/x-date-pickers'
    ),
    'material-react-table': path.resolve(
      __dirname,
      '../your-extensions/node_plugin/node_modules/material-react-table'
    ),
    'material-react-table/locales/vi': path.resolve(
      __dirname,
      '../your-extensions/node_plugin/node_modules/material-react-table/locales/vi'
    ),
    '@mui/x-date-pickers/AdapterDayjs': path.resolve(
      __dirname,
      '../your-extensions/node_plugin/node_modules/@mui/x-date-pickers/AdapterDayjs'
    ),
  });

  return webpackConfig;
};
