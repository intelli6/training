import { ImmutableObject } from 'seamless-immutable';

export type Config = {
  outputStoreKey: string;
  outputStoreSectionKey: string;
  dmaValueKey: string;
  dmaNameKey: string;
};

export type IMConfig = ImmutableObject<Config>;
