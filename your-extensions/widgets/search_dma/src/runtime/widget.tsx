import { Autocomplete, Button, TextField } from '@mui/material';
import { MapViewManager } from 'jimu-arcgis';
import {
  AllWidgetProps,
  appActions,
  DataSourceComponent,
  DataSourceManager,
  FeatureLayerDataSource,
  getAppStore,
  QueriableDataSource,
  QueryResult,
  React,
  ReactRedux,
  SqlQueryParams,
} from 'jimu-core';

import {
  mergeGeometry,
  projectPolygonGeometry,
  retrieveDataInProxy,
  _getActiveViewId,
  getJimuMapView,
  showAreaOnMap,
} from '../../../common/functions';
import {
  IDDMA,
  ProjectGeocodeURL,
  targetMapWidgetId,
  eStoreKey,
  eStorePageKey,
  TenDMA,
} from '../../../common/storeKey';

import { type IMConfig } from '../config';
import { IPolygon } from '@esri/arcgis-rest-types';
import { AreaQuery, eQueryMode } from '../../../common/sampleQuery';

const { useEffect, useRef, useState } = React;
const { useSelector } = ReactRedux;
const interval = 300;

// config dataSource Index
enum eDataSourceIndex {
  dma = 0,
}

const Widget = (props: AllWidgetProps<IMConfig>) => {
  const currentQuery = useSelector(
    () =>
      getAppStore().getState().widgetsState?.[eStorePageKey?.dma]?.[
        eStoreKey?.dataDMA
      ]
  );
  const highlightHandler = useRef(null);
  const _viewManager = useRef(MapViewManager.getInstance());
  const dataSourceRef = useRef<QueriableDataSource>(null);
  const [loading, setIsLoading] = useState(true);
  const [data, setData] = useState(currentQuery?.query || []);
  const [selectedDMA, setSelectedDMA] = useState<string>(null);
  // query param
  function generateQueryParams(geometry = false) {
    return {
      where: `${IDDMA} is not null AND ${IDDMA} <> '' AND ${TenDMA} is not null AND ${TenDMA} <> ''`,
      returnGeometry: geometry,
      outFields: [`${IDDMA},${TenDMA}`],
    };
  }
  // query data
  function queryData(dataSource: QueriableDataSource, returnGeometry = false) {
    try {
      return dataSource.query(
        generateQueryParams(returnGeometry) as SqlQueryParams
      );
    } catch (error) {
      return [];
    }
  }

  let timeOut = null as any;
  // get data DMA
  async function getDs() {
    setIsLoading(true);
    const dmaDs = DataSourceManager.getInstance().getDataSource(
      props.useDataSources?.[eDataSourceIndex.dma]?.dataSourceId
    );

    if (dmaDs !== undefined) {
      dataSourceRef.current = dmaDs as QueriableDataSource;

      clearTimeout(timeOut);
      const data = (await queryData(
        dmaDs as QueriableDataSource
      )) as QueryResult;
      const records = data.records
        ? data?.records.map((record) => retrieveDataInProxy(record))
        : [];

      if (records) {
        const res = records.filter((i) => i.IDDMA != '');
        if (res) {
          setData(res);
        }
      }
      setIsLoading(false);
    } else {
      setIsLoading(false);
      timeOut = setTimeout(() => getDs(), interval);
    }
  }
  // search , select DMA
  const handleChangeDMA = (
    _: React.ChangeEvent<HTMLInputElement>,
    data: {
      [IDDMA]: string;
      [TenDMA]: string;
    }
  ) => {
    if (data) {
      setSelectedDMA(data?.[IDDMA]);
    }
  };
  //get data 2
  function _getFeatureLayerDataSource(dataSourceId: string) {
    return DataSourceManager.getInstance().getDataSource(dataSourceId) as
      | FeatureLayerDataSource
      | undefined;
  }

  // search DMA & map
  const handleSearch = async () => {
    if (!selectedDMA) return;

    setSelectedDMA(null);
    //
    const dmaDs = _getFeatureLayerDataSource(
      props.useDataSources?.[eDataSourceIndex.dma]?.dataSourceId
    );

    if (!dmaDs) {
      // setError(eVHVError.noDataSource);
      return;
    }

    // const res = await (dmaDs as QueriableDataSource).query(
    //   generateQueryParams(true) as SqlQueryParams & FeatureLayerQueryParams
    // );

    const res = (await queryData(
      dmaDs as QueriableDataSource,
      true
    )) as QueryResult;

    const geometryDMA = res.records.find(
      (record) => retrieveDataInProxy(record)?.[IDDMA] === selectedDMA
    );

    const geometry = geometryDMA?.getGeometry() as IPolygon;

    const jMapView = await getJimuMapView(
      targetMapWidgetId,
      _viewManager.current
    );

    const { geometries } = await projectPolygonGeometry(
      ProjectGeocodeURL,
      geometry?.spatialReference,
      jMapView?.view?.spatialReference,
      geometry?.rings
    );

    if (!geometries) {
      // setError(eVHVError.noGeometry);
      return;
    }
    const mergedGeometry = mergeGeometry(
      geometries?.map((geo) => ({
        type: 'point',
        ...geo,
        spatialReference: jMapView.view.spatialReference,
      }))
    );

    if (!mergedGeometry) {
      // setError(eVHVError.canNotMergeGeometry);
      return;
    }
    _exportQueryToStore(mergedGeometry);

    // zoom to the merged geometry
    await jMapView.view.goTo({ target: mergedGeometry } ?? {}, {});
    // highlight the feature
    if (highlightHandler.current) {
      highlightHandler.current?.remove();
    }

    showAreaOnMap(jMapView, mergedGeometry as __esri.Polygon);
  };

  // init dataSource to render
  useEffect(() => {
    clearTimeout(timeOut);
    getDs();
  }, []);

  function _exportQueryToStore(selectedGeometry: __esri.Geometry) {
    const { dispatch } = props;
    const { outputStoreKey, outputStoreSectionKey } = props.config;
    console.log('selectedDMA', selectedGeometry.toJSON());
    // detect area
    if (selectedDMA) {
      dispatch(
        appActions.widgetStatePropChange(
          outputStoreKey,
          outputStoreSectionKey,
          {
            mode: eQueryMode.spatial,
            where: '1=1',
            geometry: selectedGeometry.toJSON(),
            geometryType: 'polygon',
            spatialReference: selectedGeometry?.spatialReference,
            DMA: [selectedDMA],
            Quan: [],
            Phuong: [],
          } as AreaQuery
        )
      );
      return;
    }

    if (!selectedDMA) {
      dispatch(
        appActions.widgetStatePropChange(
          outputStoreKey,
          outputStoreSectionKey,
          {
            mode: eQueryMode.spatial,
            where: '1=1',

            spatialReference: null,
            DMA: [],
            Quan: [],
            Phuong: [],
          } as AreaQuery
        )
      );
      return;
    }
  }

  return (
    <>
      <DataSourceComponent
        widgetId={props.id}
        useDataSource={props.useDataSources?.[eDataSourceIndex.dma]}
      />
      {loading ? null : (
        <div
          className="d-flex h-100 flex-row justify-content-end align-items-center"
          style={{ gap: '12px' }}
        >
          {data?.length == 0 ? null : (
            <Autocomplete
              style={{ width: 250 }}
              size="small"
              options={data}
              getOptionLabel={(option: { [IDDMA]: string; [TenDMA]: string }) =>
                option?.[TenDMA]
              }
              renderInput={(params) => (
                <TextField
                  size="small"
                  {...params}
                  label="DMA"
                  variant="outlined"
                />
              )}
              onChange={handleChangeDMA}
            />
          )}

          <Button
            disabled={!selectedDMA}
            sx={{ textTransform: 'none', fontWeight: 'bold' }}
            variant="contained"
            color="primary"
            className="px-4"
            onClick={handleSearch}
          >
            Tìm kiếm
          </Button>
          {/* <DateTimePicker label="Basic date time picker" /> */}
        </div>
      )}
    </>
  );
};

export default Widget;
