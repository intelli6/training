import { Geometry } from '@arcgis/core/geometry';
import SpatialReference from '@arcgis/core/geometry/SpatialReference';

export enum eQueryMode {
  spatial = 1,
  attributes = 2,
}
export type AreaOption = {
  attributes: {
    [key: string]: number | string;
  };
  geometry: Geometry;
};
// type of report query store in redux
export type AreaQuery = {
  mode: eQueryMode.spatial | eQueryMode.attributes;

  // Tồn tại khi mode = attributes
  where?: string; // Nếu không lấy được thì mặc định là "1 = 1"

  geometry?: Geometry; // Tồn tại khi mode = spatial

  geometryType?: string; // Nếu không lấy được thì mặc định là "polygon"

  spatialReference?: SpatialReference; // Tồn tại khi mode = spatial

  DMA: string[];

  Quan: string[];

  Phuong: string[];
};

export const sampleSpatialQueryObject = {
  report: {
    query: {
      mode: eQueryMode.spatial,
      where: '1=1',
      geometry: {
        rings: [
          [
            [600059.5504000001, 1189402.0051000006],
            [600052.6973000001, 1189444.1185999997],
            [599999.1249000002, 1189434.5438],
            [599995.4764999999, 1189453.3263000008],
            [600048.0937000001, 1189461.9056000002],
            [600023.0881000003, 1189621.4324999992],
            [600290.4312000005, 1189677.7544999998],
            [600387.9933000002, 1189696.9086000007],
            [600377.9172999999, 1189466.2029],
            [600059.5504000001, 1189402.0051000006],
          ],
        ],
      },
      geometryType: 'polygon',
      spatialReference: {
        wkt: 'PROJCS["TPHCM_VN2000",GEOGCS["GCS_VN_2000",DATUM["D_Vietnam_2000",SPHEROID["WGS_1984",6378137.0,298.257223563]],PRIMEM["Greenwich",0.0],UNIT["Degree",0.0174532925199433]],PROJECTION["Transverse_Mercator"],PARAMETER["False_Easting",500000.0],PARAMETER["False_Northing",0.0],PARAMETER["Central_Meridian",105.75],PARAMETER["Scale_Factor",0.9999],PARAMETER["Latitude_Of_Origin",0.0],UNIT["Meter",1.0]]',
      },

      // query theo spatial không cần dùng các field dưới
      // DMA: ['Q5-0801'],
      DMA: [],
      // Quan: ['774'],
      Quan: [],
      // Phuong: ['77427328', '77427304', '77437316'],
      Phuong: [],
    } as AreaQuery,
  },
};
