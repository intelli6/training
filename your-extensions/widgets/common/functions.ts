import Graphic from '@arcgis/core/Graphic';
import geometryEngine from '@arcgis/core/geometry/geometryEngine';
import { JimuMapView, MapViewManager } from 'jimu-arcgis';
import {
  DataRecord,
  ImmutableObject,
  JimuMapViewInfo,
  getAppStore,
} from 'jimu-core';
import { ProjectGeocodeURL } from './storeKey';
import { interval } from './constants';

//#region map related
export function _getActiveViewId(
  mapWidgetId: string,
  infos: ImmutableObject<{ [jimuMapViewId: string]: JimuMapViewInfo }>
): string {
  let activeViewId = Object.keys(infos || {}).find(
    (viewId) =>
      infos[viewId].mapWidgetId === mapWidgetId && infos[viewId].isActive
  );
  if (!activeViewId) {
    activeViewId = Object.keys(infos || {}).find(
      (viewId) => infos[viewId].mapWidgetId === mapWidgetId
    );
  }
  return activeViewId;
}

export async function getJimuMapView(
  mapWidgetId: string,
  _viewManager: MapViewManager
) {
  const activeViewId = _getActiveViewId(
    mapWidgetId,
    getAppStore().getState().jimuMapViewsInfo
  );
  const jimuMapView: JimuMapView =
    _viewManager.getJimuMapViewById(activeViewId);

  return await jimuMapView?.whenJimuMapViewLoaded();
}

export async function highlight_point_on_map_by_graphic(
  mapWidgetId: string,
  _viewManager: MapViewManager,
  geometry: __esri.Geometry,
  delay: number = 300
) {
  const mapView = await getJimuMapView(mapWidgetId, _viewManager);
  /*************************
   * Create a point graphic
   *************************/
  // remove the existing graphic
  mapView?.view?.graphics?.removeAll();

  // First create a point geometry (this is the location of the Titanic)
  const point = {
    type: 'point', // autocasts as new Point()
    ...geometry,
  };

  // Create a symbol for drawing the point
  const markerSymbol = {
    type: 'simple-marker', // autocasts as new SimpleMarkerSymbol()
    color: [0, 255, 255, 0.5],
    outline: {
      // autocasts as new SimpleLineSymbol()
      color: [0, 255, 255],
      width: 1.5,
    },
  };

  // Create a graphic and add the geometry and symbol to it
  const pointGraphic = new Graphic({
    geometry: point as __esri.Point,
    symbol: markerSymbol,
  });

  // Add the graphic to the view with delay
  setTimeout(() => {
    mapView?.view?.graphics?.add(pointGraphic);
  }, delay);
}

export async function clearMapSelectionAndHighlight(
  mapWidgetId: string,
  _viewManager: any
) {
  const mapView = await getJimuMapView(mapWidgetId, _viewManager);

  // for graphic only
  mapView?.clearSelectedFeatures();
  mapView?.view?.graphics?.removeAll();
}

export async function projectPointGeometry(
  url: string,
  inputSpatialReference: any,
  outputSpatialReference: any,
  geometry: any
) {
  const formData = new FormData();
  formData.append('inSR', JSON.stringify(inputSpatialReference));
  formData.append('outSR', JSON.stringify(outputSpatialReference));
  formData.append('geometries', `${geometry?.x},${geometry?.y}`);
  formData.append('transformation', '');
  formData.append('transformForward', 'true');
  formData.append('vertical', 'false');
  formData.append('f', 'pjson');
  formData.append('token', getAppStore().getState().token);

  const response = await fetch(url, {
    method: 'POST',
    body: formData,
  });

  return response.json();
}

export async function projectPolygonGeometry(
  url: string,
  inputSpatialReference: any,
  outputSpatialReference: any,
  geometry: any
) {
  const formData = new FormData();
  formData.append('inSR', JSON.stringify(inputSpatialReference));
  formData.append('outSR', JSON.stringify(outputSpatialReference));
  // formData.append('geometries', `${geometry?.x},${geometry?.y}`);
  formData.append(
    'geometries',
    `{
    "geometryType": "esriGeometryPolygon",
    "geometries": [
      {
        "rings" : ${JSON.stringify(geometry)}
      }
    ]
  }`
  );
  formData.append('transformation', '');
  formData.append('transformForward', 'true');
  formData.append('vertical', 'false');
  formData.append('f', 'pjson');
  formData.append('token', getAppStore().getState().token);

  const response = await fetch(url, {
    method: 'POST',
    body: formData,
  });

  return response.json();
}

export const mergeGeometry = (geometryArr: any[]) => {
  let result: __esri.Geometry;
  result = geometryEngine.union(geometryArr);
  return result;
};

export function retrieveDataInProxy(record: DataRecord) {
  const result: {
    [key: string]: any;
  } = {};
  const proxy = record.getData();
  for (let key in proxy) {
    result[key] = proxy[key];
  }
  return result;
}

export function zoomToMapByExtent(
  extent: __esri.Extent,
  _viewManager: MapViewManager,
  mapWidgetId: string
) {
  const mapView = getJimuMapView(mapWidgetId, _viewManager);
  mapView.then((jmv) => {
    jmv.view.goTo(extent);
  });
}

export async function showAreaOnMap(
  jimuMapView: JimuMapView,
  geometry: __esri.Polygon
) {
  if (!jimuMapView || !geometry) return;
  jimuMapView.view.graphics.removeAll();
  const { geometries } = await projectPolygonGeometry(
    ProjectGeocodeURL,
    geometry?.spatialReference,
    jimuMapView?.view?.spatialReference,
    geometry.rings
  );
  // Create a symbol for rendering the graphic
  const polygon = {
    type: 'polygon', // autocasts as new Polygon()
    rings: geometries?.[0].rings?.[0],
    spatialReference: jimuMapView.view.spatialReference,
  };

  // Create a symbol for rendering the graphic
  const fillSymbol = {
    type: 'simple-fill', // autocasts as new SimpleFillSymbol()
    color: [153, 204, 255, 0.3],
    outline: {
      // autocasts as new SimpleLineSymbol()
      cap: 'round',
      color: [230, 233, 22, 0.8],
      join: 'round',
      miterLimit: 1,
      width: 4,
    },
  };
  // Add the geometry and symbol to a new graphic
  const polygonGraphic = new Graphic({
    geometry: polygon,
    symbol: fillSymbol,
  });

  // Add the graphics to the view's graphics layer
  const blinkTime = 3;
  jimuMapView.view.graphics.add(polygonGraphic);
  for (let i = 0; i < blinkTime; i++) {
    await new Promise((resolve) => setTimeout(resolve, interval));
    jimuMapView.view.graphics.remove(polygonGraphic);
    await new Promise((resolve) => setTimeout(resolve, interval));
    jimuMapView.view.graphics.add(polygonGraphic);
  }
}

//#endregion

//#region math
export function mathRound(rawNumber: number, roundedNumber: number = 2) {
  return (
    Math.round((rawNumber + Number.EPSILON) * Math.pow(10, roundedNumber)) /
    Math.pow(10, roundedNumber)
  );
}
export function numberWithCommas(n: number) {
  if (n == null) return '';
  var parts = n.toString().split('.');
  return (
    parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',') +
    (parts[1] ? '.' + parts[1] : '')
  );
}

//#endregion

//#region event
export function createEventListener<T>(
  event: string,
  callback: (data: T) => void
): EventListenerOrEventListenerObject {
  const listener = (e: CustomEvent<T>) => callback(e.detail);
  document.addEventListener(event, listener);
  return listener;
}

export function dispatchEvent(event: string, data: any) {
  document.dispatchEvent(new CustomEvent(event, { detail: data }));
}

export function removeEventListener(
  event: string,
  callback: EventListenerOrEventListenerObject
) {
  document.removeEventListener(event, callback);
}
//#endregion
