import { IField } from '@esri/arcgis-rest-types';
import { React, lodash } from 'jimu-core';
import {
  type MRT_ColumnDef,
  type MRT_Cell,
  type MRT_Column,
} from 'material-react-table';
import { OBJECTIDKey } from './storeKey';
import { MRT_Localization_VI } from '../../node_plugin/node_modules/material-react-table/locales/vi';
// import { format } from 'date-fns';
import dayjs from 'dayjs';
import { mathRound, numberWithCommas } from './functions';

const { useMemo } = React;

const FILTER_VARIANT_TYPE = {
  ['multi']: 'multi-select',
};

const generateFilterVariant = (field: IField, isHasSubType: boolean) => {
  if (field.type === 'esriFieldTypeDate') {
    return 'date';
  }

  if (isHasSubType || field.domain) {
    return 'multi-select';
  }

  return 'text';
};

// Hooks tạo columns cho các trường được chọn đơn giản
export const useColumns = ({
  fields,
  selectedFields,
  typeIdField,
  subTypes,
}: {
  fields: IField[];
  selectedFields: any[];
  typeIdField: string;
  subTypes: any[];
}) => {
  return useMemo<MRT_ColumnDef<any>[]>(() => {
    return [
      ...generateArcGISFieldColumns(
        fields,
        selectedFields,
        typeIdField,
        subTypes
      ),
    ];
  }, [fields]);
};

// Hàm tạo columns cho các trường được chọn cho việc custom, thêm trường kết hợp, actions
export function generateArcGISFieldColumns(
  fields: IField[],
  selectedFields: string[],
  typeIdField: string,
  subTypes: any[]
) {
  return lodash.cloneDeep(
    selectedFields
      ?.map((fieldName) => {
        const field = fields.find((f) => f.name === fieldName);
        const isHasSubType =
          typeIdField && subTypes && field?.name === typeIdField;
        const isHasDomain = Boolean(field?.domain);
        const convertDomains = field?.domain?.codedValues?.map((item) => ({
          label: item.name,
          value: item.code,
        }));

        const convertSubTypes = subTypes?.map((item) => ({
          label: item.name,
          value: item.code,
        }));

        if (!field) return null;
        const filterVariant = generateFilterVariant(field, isHasSubType) as any;
        return {
          accessorKey: field.name,
          header: field.alias,
          accessorFn:
            field.type === 'esriFieldTypeDate'
              ? (originalRow: any) =>
                  originalRow[field.name]
                    ? new Date(originalRow[field.name])
                    : ''
              : null,
          filterVariant: generateFilterVariant(field, isHasSubType) as any,
          enableColumnFilterModes:
            field.type === 'esriFieldTypeDate' ? true : false,
          columnFilterModeOptions:
            field.type === 'esriFieldTypeDate'
              ? ['between', 'lessThanOrEqualTo', 'greaterThanOrEqualTo']
              : [],
          filterFn:
            field.type === 'esriFieldTypeDate' ? 'lessThanOrEqualTo' : null,
          filterSelectOptions: field?.domain
            ? convertDomains
            : isHasSubType
            ? convertSubTypes
            : [],
          visibleInShowHideMenu: field.name?.toLowerCase() !== 'objectid',
          Cell: ({
            cell,
            column,
          }: {
            cell: MRT_Cell<any>;
            column: MRT_Column<any>;
          }) => {
            // check if the field has domain
            if (isHasDomain) {
              const domain = fields.find(
                (f) => f.name === column.columnDef.id
              )?.domain;
              const domainValue = domain?.codedValues.find(
                (codedValue) => codedValue.code === cell.getValue()
              );
              return domainValue?.name;
            }

            //subtype
            if (isHasSubType) {
              return subTypes.find((item) => item.code === cell.getValue())
                ?.name;
            }

            // other types: date, number, ...
            if (field.type === 'esriFieldTypeDate') {
              return cell.getValue()
                ? dayjs(Number(cell.getValue() ?? 0)).format('DD/MM/YYYY')
                : '';
            }

            if (
              field.type === 'esriFieldTypeDouble' ||
              field.type === 'esriFieldTypeInteger'
            ) {
              return numberWithCommas(mathRound(Number(cell.getValue()), 2));
            }
            return cell.getValue();
          },
        };
      })
      .filter((column) => column !== null) ?? []
  );
}

export const commonMRTSettings = {
  enableGlobalFilter: false,
  enableStickyHeader: true,
  enableFullScreenToggle: false,
  enableColumnActions: false,
  // localization
  localization: MRT_Localization_VI,
} as any;

export const commonMRTInitStates = {
  density: 'compact',
  columnVisibility: {
    [OBJECTIDKey]: false,
  },
} as any;
