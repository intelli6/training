export enum eStorePageKey {
  dma = 'analysis',
  search = 'search',
}

export enum eStoreKey {
  dataDMA = 'dataDMA',
  dataSearch = 'dataSearch',
}

export const IDDMA = 'MADMA';
export const TenDMA = 'TENDMA';
export const targetMapWidgetId = 'widget_4';
export const ProjectGeocodeURL =
  'https://cloud.intelli.com.vn/server/rest/services/Utilities/Geometry/GeometryServer/project';
export const OBJECTIDKey = 'OBJECTID';
