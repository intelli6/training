import { type ImmutableObject } from 'seamless-immutable';

export interface Config {
  selectedFields: string[];
  subscribedStoreKey: string;
  subscribedStoreSectionKey: string;
}

export type IMConfig = ImmutableObject<Config>;
