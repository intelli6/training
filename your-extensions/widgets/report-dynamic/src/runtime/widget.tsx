import {
  React,
  type AllWidgetProps,
  DataSourceComponent,
  DataSourceManager,
  DataSource,
  ReactRedux,
  IMState,
  FeatureLayerDataSource,
  FeatureLayerQueryParams,
  QueriableDataSource,
  QueryParams,
  SqlQueryParams,
  getAppStore,
} from 'jimu-core';
import {
  MaterialReactTable,
  useMaterialReactTable,
  type MRT_ColumnDef,
  type MRT_PaginationState,
  type MRT_SortingState,
  type MRT_ColumnFiltersState,
  type MRT_FilterOption,
  type MRT_ColumnFilterFnsState,
} from 'material-react-table';
import { type IMConfig } from '../config';
import { interval } from '../../../common/constants';
import { useRef } from 'react';
import { commonMRTInitStates, useColumns } from '../../../common/mRTable';
import { MRT_Localization_VI } from 'material-react-table/locales/vi';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import {
  AreaQuery,
  eQueryMode,
  sampleSpatialQueryObject,
} from '../../../common/sampleQuery';
import dayjs from 'dayjs';
import { darken, lighten, useTheme } from '@mui/material';

const { useEffect, useState } = React;
const { useSelector } = ReactRedux;
const Widget = (props: AllWidgetProps<IMConfig>) => {
  const {
    subscribedStoreSectionKey,
    subscribedStoreKey,
    selectedFields: selectedReports,
  } = props.config;
  const theme = useTheme();
  const baseBackgroundColor =
    theme.palette.mode === 'dark'
      ? 'rgba(204, 204, 204, 1)'
      : 'rgba(255, 255, 255, 1)';
  const { config } = props;
  const appToken = useSelector((state: IMState) => state.token);
  const [isDsLoaded, setIsDsLoaded] = useState(false);
  const [fields, setFields] = useState([]);
  const [typeIdField, setTypeField] = useState<string>('');
  const [subTypes, setSubTypes] = useState([]);
  const [featureDataList, setFeatureDataList] = useState([]);

  const [countFeatureData, setCountFeatureData] = useState(0);
  const [sorting, setSorting] = useState<MRT_SortingState>([]);
  const [pagination, setPagination] = useState<MRT_PaginationState>({
    pageIndex: 0,
    pageSize: 20,
  });
  //table state
  const [columnFilters, setColumnFilters] = useState<MRT_ColumnFiltersState>(
    []
  );

  const [columnFilterFns, setColumnFilterFns] = //filter modes
    useState<MRT_ColumnFilterFnsState>(); //default to "contains" for all columns

  const [isRefetching, setIsRefetching] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [showColumnFilters, setShowColumnFilters] = useState(false);

  const titlePage = useRef(null);

  const fetchData = async () => {
    const ds: DataSource = dataSourceRef.current?.[0];
    if (!ds) return;
    titlePage.current = ds?.getLabel();
    if (!featureDataList.length) {
      setIsLoading(true);
    } else {
      setIsRefetching(true);
    }

    const areaQueryState = getAppStore().getState().widgetsState?.[
      subscribedStoreKey
    ]?.[subscribedStoreSectionKey] as AreaQuery;

    const queryParams = generateQueryParams({
      pagination,
      sorting,
      columnFilters,
      query: areaQueryState ?? sampleSpatialQueryObject.report.query,
      outFields: selectedReports.asMutable(),
    });

    // query count
    const countQueryParams = structuredClone(queryParams);
    // remove pagination from query params when query count and add spatialRel
    delete countQueryParams.page;
    delete countQueryParams.pageSize;
    delete (countQueryParams as any).resultOffset;
    delete (countQueryParams as any).resultRecordCount;
    countQueryParams.spatialRel = 'esriSpatialRelIntersects';
    const { count } = await (ds as QueriableDataSource).queryCount(
      countQueryParams
    );

    // console.log('count', count, countQueryParams);

    setCountFeatureData(count);

    // console.log(cloneQueryParams);
    // console.log('count', count, pagination.pageSize);

    if (count < pagination.pageSize) {
      queryParams.pageSize = count;
      queryParams.page = 1;
      (queryParams as any).resultOffset = 0;
      (queryParams as any).resultRecordCount = count;
    }

    // console.log(queryParams);
    (ds as QueriableDataSource)
      .query(queryParams)
      .then((queryResult) => {
        // queryResult.records.map((record) => {
        // })

        setFeatureDataList(
          queryResult.records.map((record) => record.getData())
        );

        // loading
        setTimeout(() => setIsLoading(false), interval);
        setTimeout(() => setIsRefetching(false), interval);
        setTimeout(() => setIsError(false), interval);
      })
      .catch((err) => {
        console.error('Query error:', err);
        setIsError(true);
        setIsLoading(false);
        setIsRefetching(false);
      });
  };

  const selectedDma = useSelector((state: IMState) => {
    const { subscribedStoreKey, subscribedStoreSectionKey } = props.config;
    return (
      state.widgetsState[subscribedStoreKey]?.[
        subscribedStoreSectionKey
      ] as AreaQuery
    )?.DMA;
  });

  useEffect(() => {
    fetchData();
  }, [
    pagination.pageIndex,
    pagination.pageSize,
    sorting,
    columnFilters,
    columnFilterFns,
  ]);

  useEffect(() => {
    if (selectedDma !== undefined) {
      fetchData();
    }
  }, [selectedDma]);

  useEffect(() => {
    if (!showColumnFilters) {
      columnFilters.length && setColumnFilters([]);
    }
  }, [showColumnFilters]);

  const buildSorting = (sorting: MRT_SortingState) => {
    let result = [];
    for (let sort of sorting) {
      const contidion = sort.desc ? `${sort.id} DESC` : `${sort.id} ASC`;
      result.push(contidion);
    }
    return result;
  };

  function buildWhere({
    columnFilters,
  }: {
    columnFilters: MRT_ColumnFiltersState;
  }) {
    let filters = columnFilters ?? [];
    const conditions = filters.map((item) => {
      // render where for multiple select option
      const getTypeOfFilterItem = fields.find(
        (field) => field.name === item.id
      );

      // handle filter by subtype and domain
      if (
        Array.isArray(item.value) &&
        getTypeOfFilterItem.type !== 'esriFieldTypeDate'
      ) {
        if (!item.value.length) return;
        let isNumber = false;
        if (getTypeOfFilterItem.type === 'esriFieldTypeSmallInteger') {
          isNumber = true;
        }
        const childContition = item.value
          ?.map((itemValue) =>
            isNumber
              ? `${item.id} = ${Number(itemValue)}`
              : `${item.id} = '${itemValue}'`
          )
          ?.join(' OR ');
        return `( ${childContition} )`;
      }

      // filter by one day
      if (getTypeOfFilterItem.type === 'esriFieldTypeDate') {
        const fiilterMode = columnFilterFns[item.id];
        if (fiilterMode === 'lessThanOrEqualTo') {
          const inputDateFilter = dayjs(
            !item.value ? new Date() : (item.value as any)
          )
            .set('hour', 23)
            .set('minute', 59)
            .set('second', 59)
            .format('YYYY-MM-DD HH:mm:ss');
          return item.id && `( ${item.id} <= TIMESTAMP '${inputDateFilter}' )`;
        }
        if (fiilterMode === 'greaterThanOrEqualTo') {
          const inputDateFilter = dayjs(
            !item.value ? new Date() : (item.value as any)
          )
            .set('hour', 0)
            .set('minute', 0)
            .set('second', 0)
            .format('YYYY-MM-DD HH:mm:ss');
          return item.id && `( ${item.id} >= TIMESTAMP '${inputDateFilter}' )`;
        }
        if (fiilterMode === 'between') {
          const [start, end]: any = item.value;
          if (!start && !end) return;
          if (!start && end) return;
          if (start && !end) return;
          const formattedDateStart = dayjs(!start ? 0 : (start as any))
            .set('hour', 0)
            .set('minute', 0)
            .set('second', 0)
            .format('YYYY-MM-DD HH:mm:ss');

          const formattedDateEnd = dayjs(!end ? new Date() : (end as any))
            .set('hour', 23)
            .set('minute', 59)
            .set('second', 59)
            .format('YYYY-MM-DD HH:mm:ss');
          return (
            item.id &&
            `( ${item.id} BETWEEN TIMESTAMP '${formattedDateStart}' and TIMESTAMP '${formattedDateEnd}')`
          );
        }
      }

      // filter by number
      if (
        getTypeOfFilterItem &&
        (getTypeOfFilterItem.type === 'esriFieldTypeDouble' ||
          getTypeOfFilterItem.type === 'esriFieldTypeInteger')
      ) {
        return item.id && `( ${item.id} = ${Number(item.value)} )`;
      }

      return item.id && `( ${item.id} LIKE N'%${item.value}%' )`;
    });
    let where = conditions
      .filter((condition) => condition != null && condition !== '')
      .join(' AND ');
    return where;
  }

  function generateQueryParams({
    pagination,
    sorting,
    columnFilters,
    query,
    outFields,
  }: {
    pagination: MRT_PaginationState;
    sorting: MRT_SortingState;
    columnFilters: MRT_ColumnFiltersState;
    query: AreaQuery;
    outFields: string[];
  }) {
    // use for join when buildwhere = null
    const commonWhere = '1 = 1';
    const conditions = [commonWhere];
    conditions.push(buildWhere({ columnFilters }));

    // fillter null
    const where = conditions
      .filter((condition) => condition !== null && condition !== '')
      .join(' AND ');
    const sqlQueryObj: QueryParams & FeatureLayerQueryParams & SqlQueryParams =
      {
        // where:
        //   query.mode === eQueryMode.attributes
        //     ? columnFilters.length
        //       ? where
        //       : `1=1`
        //     : null, //TODO change null for mode spatial
        where: columnFilters?.length ? where : `1=1`,
        outFields: outFields?.length ? outFields : ['*'],
        //sorting props
        orderByFields: sorting?.length ? buildSorting(sorting) : [],
        // pagination props
        page: pagination.pageIndex + 1,
        pageSize: pagination.pageSize,
        resultType: 'standard',
        resultRecordCount: pagination.pageSize,
        returnExceededLimitFeatures: true,
        resultOffset: pagination.pageIndex * pagination.pageSize,

        // returnGeometry: query.mode === eQueryMode.spatial ? true : false,
        returnGeometry: false,
        // Todo add property for filter spatial
      } as any;

    if (query.mode === eQueryMode.spatial) {
      const selectedDma = query?.DMA;
      const selectedDistrict = query?.Quan;
      const selectedWard = query?.Phuong;
      // detect query all
      if (
        selectedDma?.length +
          selectedDistrict?.length +
          selectedWard?.length ===
        0
      ) {
        // sqlQueryObj.where = `${commonWhere}`;
        // temporary fix for smaller area to query fast
        // sqlQueryObj.where = `${commonWhere} AND MADMA = 'Q5-0501-0601'`;
      } else {
        // special case for dma query because dma can be overlap in geometry query
        sqlQueryObj.geometry = query.geometry;
        sqlQueryObj.geometryType = 'esriGeometryPolygon';
        // sqlQueryObj.where = `${query.where}`;
        sqlQueryObj.spatialRel = 'esriSpatialRelIndexIntersects';
      }
    }
    // console.log(sqlQueryObj);
    return sqlQueryObj;
  }

  const dataSourceRef = useRef<DataSource[]>();

  const columns = useColumns({
    fields: fields,
    selectedFields: config.selectedFields as unknown as string[],
    typeIdField,
    subTypes,
  });

  useEffect(() => {
    setColumnFilterFns(
      Object.fromEntries(
        columns.map(({ accessorKey }) => [accessorKey, 'lessThanOrEqualTo'])
      )
    );
  }, [columns]);

  let timeOut: any = null;
  function getDs() {
    // Có thể khai báo cái ref từ useRef bên trên để save ds lại để dùng về sau
    const dsArr: DataSource[] = [];
    props.useDataSources?.forEach((useDataSource) => {
      const ds = DataSourceManager.getInstance().getDataSource(
        useDataSource?.dataSourceId
      );
      dsArr.push(ds);
    });

    if (dsArr?.length && dsArr.every((ds) => ds)) {
      dataSourceRef.current = dsArr;
      const dataSourceFeature = dsArr[0] as FeatureLayerDataSource;
      const fields = dataSourceFeature.getLayerDefinition()?.fields;

      const subTypes = (
        (
          dataSourceFeature as FeatureLayerDataSource
        ).getLayerDefinition() as any
      ).subtypes;

      const typeIdField = (
        dataSourceFeature as FeatureLayerDataSource
      ).getLayerDefinition().subtypeField;
      setFields(fields);
      setTypeField(typeIdField);
      setSubTypes(subTypes);
      setIsDsLoaded(true);
      fetchData();
      clearTimeout(timeOut);
    } else {
      timeOut = setTimeout(() => getDs(), interval);
    }
  }

  useEffect(() => {
    if (!appToken) return;
    clearTimeout(timeOut);
    getDs();
  }, [appToken]);

  const table = useMaterialReactTable({
    columns,
    data: featureDataList,
    enableGlobalFilter: false,
    enableStickyHeader: true,
    enableFullScreenToggle: false,
    isMultiSortEvent: () => true, //now no need to hold `shift` key to multi-sort

    // server render
    onPaginationChange: setPagination,
    onSortingChange: setSorting,
    onColumnFiltersChange: setColumnFilters,
    onShowColumnFiltersChange: setShowColumnFilters,
    onColumnFilterFnsChange: setColumnFilterFns,

    rowCount: countFeatureData,
    manualFiltering: true, //turn off built-in client-side filtering
    manualPagination: true, //turn off built-in client-side pagination
    manualSorting: true, //turn off built-in client-side sorting
    enableColumnFilterModes: true,
    muiFilterDatePickerProps: {
      format: 'DD/MM/YYYY',
      slotProps: { textField: { style: { width: '200px' } } },
    },

    muiToolbarAlertBannerProps: isError
      ? {
          color: 'error',
          children: 'Error loading data',
        }
      : undefined,
    // localization
    localization: MRT_Localization_VI,

    displayColumnDefOptions: {
      'mrt-row-actions': {
        header: '',
      },
    },
    muiTableContainerProps: {
      sx: {
        height: 'calc(100vh - 260px)',
        display: 'flex',
        flexDirection: 'column',
      },
    },
    muiTableBodyProps: {
      sx: (theme) => ({
        '& tr:nth-of-type(odd):not([data-selected="true"]):not([data-pinned="true"]) > td':
          {
            backgroundColor: darken(baseBackgroundColor, 0.1),
          },
        '& tr:nth-of-type(odd):not([data-selected="true"]):not([data-pinned="true"]):hover > td':
          {
            backgroundColor: darken(baseBackgroundColor, 0.2),
          },
        '& tr:nth-of-type(even):not([data-selected="true"]):not([data-pinned="true"]) > td':
          {
            backgroundColor: lighten(baseBackgroundColor, 0.1),
          },
        '& tr:nth-of-type(even):not([data-selected="true"]):not([data-pinned="true"]):hover > td':
          {
            backgroundColor: darken(baseBackgroundColor, 0.2),
          },
        overflowY: 'auto',
        flexGrow: 1,
      }),
    },
    initialState: commonMRTInitStates,
    state: {
      pagination,
      sorting,
      columnFilters,
      columnFilterFns,
      showProgressBars: isRefetching,
      isLoading,
      showAlertBanner: isError,
      showColumnFilters,
    }, //pass the pagination state to the table

    mrtTheme: (theme) => ({
      baseBackgroundColor: baseBackgroundColor,
      draggingBorderColor: theme.palette.secondary.main,
    }),
  });

  return (
    <>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        {props.useDataSources?.map((useDataSource, index) => (
          <>
            <DataSourceComponent
              key={`ds-edit-valve-${index}`}
              useDataSource={useDataSource}
              widgetId={props.id}
            />
          </>
        ))}
        {isDsLoaded ? (
          <>
            {/*Render component here*/}
            <MaterialReactTable table={table} />
          </>
        ) : null}
      </LocalizationProvider>
    </>
  );
};

export default Widget;
